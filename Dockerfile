FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY /target/apikeys-0.0.1-SNAPSHOT.jar app/app.jar
EXPOSE 8090
CMD ["java", "-Xmx200m", "-jar","/app/app.jar"]
