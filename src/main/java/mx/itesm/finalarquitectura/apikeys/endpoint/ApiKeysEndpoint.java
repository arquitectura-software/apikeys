package mx.itesm.finalarquitectura.apikeys.endpoint;

import mx.itesm.finalarquitectura.apikeys.domain.ApiKey;
import mx.itesm.finalarquitectura.apikeys.pojo.Message;
import mx.itesm.finalarquitectura.apikeys.pojo.Result;
import mx.itesm.finalarquitectura.apikeys.service.ApiKeysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("")
@Produces(MediaType.APPLICATION_JSON)
public class ApiKeysEndpoint {

    @Autowired
    private ApiKeysService apiKeysService;

    @GET
    @Path("/apiKey")
    public Response getApiKey() {
        Result<ApiKey> result = apiKeysService.getKey();
        Response response;
        if(result.getData().isPresent()) {
            response = Response.ok(result.getData().get()).build();
        } else {
            response = Response.serverError().entity(new Message("Error de servidor.")).build();
        }
        return response;
    }
}
