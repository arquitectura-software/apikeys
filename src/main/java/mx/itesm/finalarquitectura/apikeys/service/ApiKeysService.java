package mx.itesm.finalarquitectura.apikeys.service;

import mx.itesm.finalarquitectura.apikeys.domain.ApiKey;
import mx.itesm.finalarquitectura.apikeys.pojo.Result;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Service
public class ApiKeysService {

    private String apiKeys []; // Google API keys
    private int currentApiKey = 0; // Used to switch between keys when one has stopped working due to the free tier

    @PostConstruct
    private void initService() {
        // New API keys can be added if needed
        apiKeys = new String[] {
                "AIzaSyAvj1kkx7ajqhYfd2oSy4wkTNQ47C5MPHA",
                "AIzaSyDHE3RD2fwu56cfB0utSFWF92g_RzqOjxY",
                "AIzaSyChC54EYNegNxJsiIGNoC8-RwYrIaqKxpM",
                "AIzaSyC39vbskLhHnGhNOWYmA_yy5dhjB25hszI",
                "AIzaSyC0MaQbGsB3abZDnSz8L3RKXUralzT_aPE",
                "AIzaSyBepmsqMx5GI27IDs3sCS_MokcA7W1Psgs",
                "AIzaSyAbwMvjyt2Iqby6UMqipcdcWvGNL_oYPvw"
        };
    }

    public Result<ApiKey> getKey() {
        Result<ApiKey> result = new Result<>();
        this.currentApiKey %= apiKeys.length;
        ApiKey apiKey = new ApiKey();
        apiKey.setKey(this.apiKeys[this.currentApiKey]);
        result.setData(Optional.of(apiKey));
        this.currentApiKey++;

        return result;
    }
}
