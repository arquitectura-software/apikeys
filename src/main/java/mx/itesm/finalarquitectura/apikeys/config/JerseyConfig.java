package mx.itesm.finalarquitectura.apikeys.config;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import mx.itesm.finalarquitectura.apikeys.endpoint.ApiKeysEndpoint;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(JacksonJaxbJsonProvider.class);
        registerEndpoints();
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
    }

    public void registerEndpoints() {
        register(ApiKeysEndpoint.class);
    }
}
